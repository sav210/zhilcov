import './App.css';
import Logo from './components/Logo/Logo';
import TicketSearch from './components/TicketSearch/TicketSearch';

function App() {
  return (
    <div className="App">
      <Logo />
      <TicketSearch />
    </div>
  )
}

export default App;
