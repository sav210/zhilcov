import React from 'react';
import './Logo.css';
import logo from '../../assets/Logo.svg';


function Logo () {
    return (
        <div className="Logo">            
            <img src={logo} alt="Logo" />
        </div>
    );
}

export default Logo;