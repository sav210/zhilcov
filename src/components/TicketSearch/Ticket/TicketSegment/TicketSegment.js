import React from 'react';
import './TicketSegment.css';

function TicketSegment(props) {
    const segment = props.segment;

    const formatStopsHeader = (stopsCounts) => {
        switch (stopsCounts) {
            case 0: return 'без пересадок';
            case 1: return '1 пересадка';
            case 2: return '2 пересадки';
            case 3: return '3 пересадки';
            default: throw new Error("Shouldn't have got here");
        }
    }

    const formatTime = (dateStr) => {
        return new Date(dateStr).toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
    }

    const addMinutes = (dateString, duraton) => {
        const takeOff = new Date(dateString);
        return new Date(takeOff.getTime() + duraton*60000).toString();    
    }

    const formatDuration = (duration) => {
        const hours = Math.floor(duration / 60);          
        const minutes = duration % 60;
        return `${hours}ч ${minutes}м`;
    }

    return (
        <div className="segment">
            <div className="segment-line segment-header">
                <div className="segment-item">{segment.origin} &#8211; {segment.destination}</div>
                <div className="segment-item">В ПУТИ</div>
                <div className="segment-item">{formatStopsHeader(segment.stops.length)}</div>
            </div>
            <div className="segment-line segment-data">
                <div className="segment-item">{formatTime(segment.date)}  &#8211; {formatTime(addMinutes(segment.date, segment.duration))}</div>
                <div className="segment-item">{formatDuration(segment.duration)}</div>
                <div className="segment-item">{segment.stops.join(', ')}</div>
            </div>
        </div>
    );
}

export default TicketSegment;