import React from 'react';
import './Ticket.css';

import TicketSegment from './TicketSegment/TicketSegment';

function Ticket (props) {
    const ticket = props.ticket;

    return <div className="ticket">
        <div className="ticket-header">
            <div className="ticket-price">{ticket.price.toLocaleString() + " Р"}</div>
            <div className="ticket-carrier">
                <img src={`https://pics.avs.io/99/36/${ticket.carrier}.png`} alt={ticket.carrier}></img>
            </div>
        </div>
        <TicketSegment segment={ticket.segments[0]}/>
        <TicketSegment segment={ticket.segments[1]}/>
    </div>
}

export default Ticket;