import React from 'react';
import './Filter.css';
import Checkbox from '../../UI/Checkbox/Checkbox';

function Filter (props) {
    const options = Object.keys(props.filter).map(key => {
        const option = props.filter[key];
        return <Checkbox 
            name={key} 
            text={option.name} 
            checked={option.value}
            changed={props.changed}
        />
    });
    
    return (
        <div className="filter">
            <div className="filterHeader">Количество пересадок</div>
            {options}
        </div>
    );
}

export default Filter;