import React from 'react';
import './SortOrderOption.css';

function SortOrderOption(props) {
    var styles = [
        "sort-order-option", 
        props.left ? "left" : "right",
        props.active ? "active" : null
    ];
    
    return <button 
        className={styles.join(' ')}
        onClick={() => props.clicked(props.value)}
        >{props.name}
    </button>
}

export default SortOrderOption;