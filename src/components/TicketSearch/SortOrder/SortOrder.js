import React from 'react';
import './SortOrder.css';
import SortOrderOption from './SortOrderOption/SortOrderOption';


function SortOrder(props) {
    return <div className="sort-order">
        <SortOrderOption 
            name="Самый дешевый"
            value="cheap" 
            active={props.value === "cheap"} 
            clicked={props.clicked}
            left
        />
        <SortOrderOption 
            name="Самый быстрый"
            value="fast" 
            active={props.value === "fast"}
            clicked={props.clicked}
        />
    </div>
}

export default SortOrder;