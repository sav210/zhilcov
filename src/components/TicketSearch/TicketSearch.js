import React, { useEffect, useState, useReducer } from 'react';

import './TicketSearch.css';
import Filter from './Filter/Filter';
import SortOrder from './SortOrder/SortOrder';
import Ticket from './Ticket/Ticket';

const defaultFilter = {
    ALL: {name: "Все", value: true},
    NO_TRANSFER: {name: "Без пересадок", value: false},
    ONE_TRANSFER: {name: "1 пересадка", value: false},
    TWO_TRANSFERS: {name: "2 пересадки", value: false},
    THREE_TRANSFERS: {name: "3 пересадки", value: false}
}

const filterReducer = (currentFilter, action) => {
    const filter = {...currentFilter};
    filter[action.type].value = !filter[action.type].value;

    if (action.type === 'ALL') {
        filter.NO_TRANSFER.value = filter.ONE_TRANSFER.value = filter.TWO_TRANSFERS.value = filter.THREE_TRANSFERS.value = false;
    } else {
        filter.ALL.value = false;
        if (! (filter.NO_TRANSFER.value || filter.ONE_TRANSFER.value || filter.TWO_TRANSFERS.value || filter.THREE_TRANSFERS.value)) {
            filter.ALL.value = true;    
        }
    }
    return filter;
}

function TicketSearch () {
    const [filter, dispatch] = useReducer(filterReducer, defaultFilter);
    const [sortOrder, setSortOrder] = useState('cheap');
    const [searchId, setSearchId] = useState('');
    const [tickets, setTickets] = useState([]);
    const [filteredTickets, setFilteredTickets] = useState([]);
    const [searchStop, setSearchStop] = useState(false);
    const [error, setError] = useState(false);

    // fetching new search session
    useEffect(() => {
        fetch('https://front-test.beta.aviasales.ru/search')
        .then(response => response.json())
        .then(response => {
            setSearchId(response.searchId);
            setSearchStop(false);
            setTickets([]);
        });
    },[]);

    // fetching tickets
    useEffect(() => {
        const timer = setTimeout(() => {
            if (error) {
                setError(null);
            } else {                
                if (searchId && !searchStop) {
                    fetch('https://front-test.beta.aviasales.ru/tickets?searchId='+searchId)
                    .then(response => response.json())
                    .then(response => {
                        setSearchStop(response.stop);
                        setTickets(tickets.concat(response.tickets));
                    })
                    .catch(error => setError(error));
                }
            }
        }, 500);
        return () => {
            clearTimeout(timer);
        };
    },[searchId, tickets, error, searchStop]);

    // filtering fetched tickets
    useEffect(() => {
        const filteredTickets = [...tickets].filter(t => {
            return filter.ALL.value
                || (filter.NO_TRANSFER.value && t.segments[0].stops.length === 0 && t.segments[1].stops.length === 0)
                || (filter.ONE_TRANSFER.value && t.segments[0].stops.length === 1 && t.segments[1].stops.length === 1)
                || (filter.TWO_TRANSFERS.value && t.segments[0].stops.length === 2 && t.segments[1].stops.length === 2)
                || (filter.THREE_TRANSFERS.value && t.segments[0].stops.length === 3 && t.segments[1].stops.length === 3)
                
        }).sort((t1, t2) => {
            if (sortOrder === 'cheap') {
                return t1.price - t2.price
            } else {
                return t1.segments[0].duration + t1.segments[1].duration
                    - t2.segments[0].duration + t2.segments[1].duration
            }
        }).slice(0, 5);
        
        setFilteredTickets(filteredTickets);
    }, [tickets, filter, sortOrder])

    const filterChanged = (key) => {
        dispatch({type: key})
    }

    return (
        <div className="ticketSearch">
            <Filter filter={filter} changed={(key) => filterChanged(key)}/>
            <div className="rightPane">
                <SortOrder value={sortOrder} clicked={(value) => setSortOrder(value)}/>
                <div>
                    {filteredTickets.map(t => {
                        return <Ticket ticket={t} />
                    })}
                </div>
            </div>
        </div>
    )
}

export default TicketSearch;