import React from 'react';
import './Checkbox.css';

function Checkbox (props) {    
    return (
        <div className="checkbox">
            <label className="container">
                <input 
                    type="checkbox" 
                    name={props.name} 
                    checked={props.checked} 
                    onChange={() => props.changed(props.name)}
                />
                <span className="checkmark"></span>                
            </label>
            <span className="checkmark-text">{props.text}</span>
        </div>
    )
}

export default Checkbox;